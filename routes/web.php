<?php

use App\Http\Controllers\accountController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\MealsController;
use App\Http\Controllers\MenusController;
use App\Http\Controllers\showallController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/account', function () {
    return view('account');
})->name('account');

Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('item',ItemsController::class)->middleware('auth');
Route::resource('meal',MealsController::class)->middleware('auth');
Route::resource('menu',MenusController::class)->middleware('auth');
Route::get('/', [App\Http\Controllers\Controller::class, 'index'])->name('home');
Route::get('/account/modification', [accountController::class,'index'])->name('modif');
Route::post('/account/modification', [accountController::class,'changepass'])->name('changepass');
Route::delete('meal/delete/{meal}', [MealsController::class, 'delete'])->name('meal.delete');
Route::get('/showall/items', [showallController::class,'showitem'])->name('showitems');
Route::get('/showall/meals', [showallController::class,'showmeals'])->name('showmeals');
Route::get('/showall/menus/', [showallController::class,'showmenu'])->name('showmenu');
Route::get('/showall/{slug}', [showallController::class,'showmenu']);
Route::get('/showall/menus/{id}', [showallController::class,'showm'])->name('showm');
Route::get('/showall/meals/{id}', [showallController::class,'showme'])->name('showme');
Route::get('/showall/items/{id}', [showallController::class,'showit'])->name('showit');
Route::post('/showall/recherche', [showallController::class,'recherche'])->name('recherche');
Route::delete('item/force/{id}', [ItemsController::class, 'forceDestroy'])->name('item.force.destroy');
Route::put('item/restore/{id}', [ItemsController::class, 'restore'])->name('item.restore');
Route::delete('meal/force/{id}', [MealsController::class, 'forceDestroy'])->name('meal.force.destroy');
Route::put('meal/restore/{id}', [MealsController::class, 'restore'])->name('meal.restore');
Route::delete('menu/force/{id}', [MenusController::class, 'forceDestroy'])->name('menu.force.destroy');
Route::put('menu/restore/{id}', [MenusController::class, 'restore'])->name('menu.restore');
