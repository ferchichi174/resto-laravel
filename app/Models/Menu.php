<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    protected $fillable = ['title', 'image', 'description','type','user_id'];

    use HasFactory,SoftDeletes;

    public function Item()
    {
        return $this->hasMany(Item::class);
    }
    public function user()
    {

        return $this->belongsTo(User::class);


    }

}
