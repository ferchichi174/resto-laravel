<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory,SoftDeletes;

    Protected $fillable =['title','description','price','image','menu_id','user_id'] ;

    public function menu(){

        return $this->belongsTo(Menu::class);


    }

    public function Meal()
    {
        return $this->belongsToMany(Meals::class);
    }

    public function user()
    {

        return $this->belongsTo(User::class);


    }


    }
