<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meals extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['title', 'image', 'description','price','user_id'];

    public function Item()
    {
        return $this->belongsToMany(Item::class);
    }
    public function user()
    {

        return $this->belongsTo(User::class);


    }
}
