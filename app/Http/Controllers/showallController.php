<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Meals;
use App\Models\Menu;
use Illuminate\Http\Request;

class showallController extends Controller
{
    public function showitem()
    {
        $items = Item::paginate(9);
        return view('itemshow',compact('items'));
    }
    public function showmeals()
    {
        $items = Meals::paginate(9);
        return view('mealshow',compact('items'));
    }

    public function showmenu($slug= null){
        if ($slug == null){
            $items = Menu::paginate(9);
        }
        else  {
        $items = Menu::where('type',$slug)->paginate(9);

        }
        return view('menushow',compact('items','slug'));
    }


    public function showm($id)
    {
        $menu = Menu::where('id',$id)->first();
        return view('Menus/show',compact('menu'));
    }

    public function showme($id)
    {
        $meals = Meals::where('id',$id)->first();
        return view('Meals/show',compact('meals'));
    }

    public function showit($id)
    {
        $item = Item::where('id',$id)->first();
        return view('Items/show',compact('item'));
    }

    public function recherche(Request $request){
        $input = $request->all();
       $items=  Item::where('title','like','%' . $input['search'] . '%')->paginate(5);
        return view('itemshow',compact('items'));
    }


}
