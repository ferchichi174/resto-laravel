<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Meals;
use App\Models\Menu;
use Illuminate\Http\Request;

class MealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meals= Meals::withTrashed()->oldest('title')->paginate(5);
        return view('Meals/index', compact('meals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menuss = Item::all();

        return view('/Meals/create',compact('menuss'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $meal = new Meals();
        $meal->title = $input['title'];
        $meal->description = $input['description'];
        $meal->price = $input['price'];
        $input["user_id"] = \Auth::user()->id;
        $meal->user_id =   $input["user_id"];
        if (isset($input['image'])){
            $meal->image =  $this->upload($input['image']) ;
        }
        $meal->save();
        $meal->item()->attach($input['itemsmenu']);
        return redirect()->route('item.index')->with('info', 'Le item a bien été créer ');
    }

    public function upload ($file){
        $extension = $file->getClientOriginalExtension();
        $sha1 = sha1($file->getClientOriginalName());
        $filename = date('Y-m-d-h-i-s').$sha1.".".$extension ;
        $path = public_path('images/');
        $file->move($path,$filename);
        return 'images/'.$filename ;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

          $meals=Meals::where('id',$id)->first();
        return view('Meals/show',compact('meals'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meals=Meals::where('id',$id)->first();
        $allItem= Item::all();
        return view('Meals/edit', compact('meals','allItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $meal = Meals::where('id',$id)->first();
        $im = $meal->image;
        $meal->title = $input['title'];
        $meal->description = $input['description'];
        $input["user_id"] = \Auth::user()->id;
        $meal->user_id =   $input["user_id"];
        if (isset($input['image'])){
            $meal->image =  $this->upload($input['image']) ;

        }
        else { $meal->image =$im;}

        $meal->update();
        $meal->item()->sync($input['itemsmenu']);
        return redirect()->route('meal.index')->with('info', 'Le Meal a bien été
modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

       Meals::findOrfail($id)->delete();
        return back()->with('info', 'Le film a bien été mis dans la corbeille.');
    }

    public function restore($id)
    {
        Meals::withTrashed()->whereId($id)->firstOrFail()->restore();
        return back()->with('info', 'Le film a bien été restauré.');
    }

    public function forceDestroy($id)
    {
        Meals::withTrashed()->whereId($id)->firstOrFail()->forceDelete();
        return back()->with('info', 'Le film a bien été supprimé définitivement dans
la base de données.');
    }
}
