<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Menu;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items= Item::withTrashed()->oldest('title')->paginate(5);
        return view('Items/index', compact('items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menuss = Menu::all();
        return view('/Items/create',compact('menuss'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $item = new Item();
        $item->title = $input['title'];
        $item->description = $input['description'];
        $item->price = $input['price'];
        $input["user_id"] = \Auth::user()->id;
        $item->user_id =   $input["user_id"];
        $item->menu_id = $input['itemsmenu'];
        if (isset($input['image'])) {
            $item->image = $this->upload($input['image']);

        }
         if (isset($input['isbest'])){
             $item->isbest =$input['isbest'];
    }
        $item->save();
        return redirect()->route('item.index')->with('info', 'Le item a bien été créer ');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {

     return view('Items/show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $menu = $item->menu->title;
        $menuss = Menu::all();
        return view('Items/edit', compact('item','menu','menuss'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $item = Item::where('id',$id)->first();
        $im = $item->image;
        $item->title = $input['title'];
        $item->description = $input['description'];
        $item->price = $input['price'];
        $input["user_id"] = \Auth::user()->id;
        $item->user_id =   $input["user_id"];
        if (isset($input['isbest'])){
            $item->isbest =$input['isbest'];
        }
        if (isset($input['image'])){
            $item->image =  $this->upload($input['image']) ;

        }
        else { $item->image =$im;}

        $item->update();

        return back()->with('info', 'Le item a bien été modifié ');

    }


    public function upload ($file){
        $extension = $file->getClientOriginalExtension();
        $sha1 = sha1($file->getClientOriginalName());
        $filename = date('Y-m-d-h-i-s').$sha1.".".$extension ;
        $path = public_path('images/');
        $file->move($path,$filename);
        return $filename ;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::findOrfail($id)->delete();
        return back()->with('info', 'Le film a bien été mis dans la corbeille.');
    }

    public function restore($id)
    {
        Item::withTrashed()->whereId($id)->firstOrFail()->restore();
        return back()->with('info', 'Le film a bien été restauré.');
    }

    public function forceDestroy($id)
    {
        Item::withTrashed()->whereId($id)->firstOrFail()->forceDelete();
        return back()->with('info', 'Le film a bien été supprimé définitivement dans
la base de données.');
    }


}
