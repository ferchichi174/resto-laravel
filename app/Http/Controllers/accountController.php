<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class accountController extends Controller
{
    public function index()
    {

        return view('modif');
    }

    public function changepass(Request $request){
        $user = Auth::user();
      $users=  $user->getAuthPassword();
        $encrypted = bcrypt($request->new_password_first);
        $use = User::where('id',$user->getAuthIdentifier())->first();
         $use->update(['password'=>$encrypted])  ;
         return back()->with('Votre mot de passe a été changé');
    }
}
