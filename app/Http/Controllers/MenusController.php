<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Meals;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Menus= Menu::withTrashed()->oldest('title')->paginate(5);
        return view('Menus/index', compact('Menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/Menus/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $menu = new Menu();
        $menu->title = $input['title'];
        $menu->description = $input['description'];
        $input["user_id"] = \Auth::user()->id;
        $menu->user_id =   $input["user_id"];
        $menu->type = $input['itemsmenu'];
        if (isset($input['image'])){
            $menu->image =  $this->upload($input['image']) ;
        }
        if (isset($input['isbest'])){
            $menu->isbest =$input['isbest'];
        }
        $menu->save();
        return redirect()->route('menu.index')->with('info', 'Le menu a bien été créer ');
    }

    public function upload ($file){
        $extension = $file->getClientOriginalExtension();
        $sha1 = sha1($file->getClientOriginalName());
        $filename = date('Y-m-d-h-i-s').$sha1.".".$extension ;
        $path = public_path('images/');
        $file->move($path,$filename);
        return 'images/'.$filename ;

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return view('Menus/show',compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('Menus/edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $menu = Menu::where('id',$id)->first();
        $im = $menu->image;
        $input["user_id"] = \Auth::user()->id;
        $menu->user_id =   $input["user_id"];
        $menu->title = $input['title'];
        $menu->description = $input['description'];
        $menu->type = $input['itemsmenu'];
        if (isset($input['image'])){
            $menu->image =  $this->upload($input['image']) ;

        }
        else { $menu->image =$im;}
        if (isset($input['isbest'])){
            $menu->isbest =$input['isbest'];
        }
        $menu->update();

        return back()->with('info', 'Le item a bien été modifié ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::findOrfail($id)->delete();
        return back()->with('info', 'Le film a bien été mis dans la corbeille.');
    }

    public function restore($id)
    {
        Menu::withTrashed()->whereId($id)->firstOrFail()->restore();
        return back()->with('info', 'Le film a bien été restauré.');
    }

    public function forceDestroy($id)
    {
        Menu::withTrashed()->whereId($id)->firstOrFail()->forceDelete();
        return back()->with('info', 'Le film a bien été supprimé définitivement dans
la base de données.');
    }
}
