@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-9">
            <h1 style="font-family:'Dancing Script', cursive  !important;font-size: 67px;
    text-align: center; "class=" animate__animated animate__backInLeft">Our Meals</h1>
            <div class="row">
                @foreach($items as $item)
                    <div class="col-md-4">
                        <div class="product-item text-center">
                            <a href="{{ route('showme',$item->id) }}" ><img src="/images/{{ $item->image}}" class="img-fluid"/></a>
                            <h5>{{ $item->title }}</h5>
                            <span class="product-subtitle">{{ $item->title}}</span>
                            <span class="product-price">{{ ( $item->price /100) |number_format(2)}} £</span>
                        </div>



                    </div>

                @endforeach

            </div>
            <div class="navigation" style="margin-top: 100px">
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
