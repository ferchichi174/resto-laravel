@extends('welcome')
@section('content')
<h1> Modifier mon Compt</h1>
<a href="/account"> Retourner a mon compt </a><br>
<hr >
<form name="change_password" method="post" action="{{ route('changepass') }}">
    @csrf
    @method('post')
    <div id="change_password">
        <div class="form-group"><label for="change_password_email" class="required">Mon adresse email</label><input type="email" id="change_password_email" name="email" disabled="disabled" required="required" class="form-control" value="{{Auth::user()->email}} " /></div>
        <div class="form-group"><label for="change_password_firstname" class="required">Mon Prenom</label><input type="text" id="change_password_firstname" name="firstname" disabled="disabled" required="required" class="form-control" value="{{Auth::user()->name}}" /></div>
        <div class="form-group"><label for="change_password_old_password" class="required">Mon mot de passe actuel</label><input type="password" id="change_password_old_password" name="oldpassword" required="required" class="form-control" /></div>
        <div class="form-group"><label for="change_password_new_password_first" class="required">Mon nouveau mot de passe</label><input type="password" id="change_password_new_password_first" name="new_password_first" required="required" class="form-control" /></div>
        <div class="form-group"><label for="change_password_new_password_second" class="required">Confirmer la nouveau mot de passe</label><input type="password" id="change_password_new_password_second" name="new_password" required="required" class="form-control" /></div>
        <div class="form-group"><button type="submit" id="change_password_submit" name="change_password" class="btn-primary btn">Submit</button></div>
    </div>
</form>
<hr >
@endsection
