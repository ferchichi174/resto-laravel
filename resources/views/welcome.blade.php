<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="La boutique qui vend uniquement du made in France.">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <title>La Restaurant Tunisienne</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/boutique.css') }}">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/carousel.css') }}" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{ route('home') }}" style="font-family:'Dancing Script', cursive  !important;font-size: 23px;">La Restaurant</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
@auth
                <li class="nav-item active">
                    <a class="nav-link test" href="{{ route('menu.index') }}">Menus</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('item.index') }}">Items</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('meal.index') }}">Meals</a>
                </li>

                @else
                <li class="nav-item active">
                    <a class="nav-link collapse navbar-collapse"  href="{{ route('showmenu') }}">Our Menus</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('showitems') }}"> Our Items</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('showmeals') }}"> Our Meals</a>
                </li>
                @endauth

            </ul>
            <form class="form-inline my-2 my-lg-0 " style="    margin-right: 17px;" action=" {{route('recherche')}}" method="post">
                @csrf
                @method('post')
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="search" id="search">
                <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div class="nav-item-custom">
                @auth
                <a href="{{ route('account') }}">Mon compte <small></small></a> | <a href="{{ url('/logout') }}">Déconnexion</a>
@else
                <a href="{{ route('login') }}">Connexion</a> | <a href="{{ route('register') }}">Inscription</a>
                @endauth
            </div>
            <a href="">
                              <img src="{{ asset('assets/img/shopping-cart.png') }}" class="cart-icon" alt="Mon panier">
                           </a>
        </div>
    </nav>
</header>


<main role="main">
 @yield('carousel')
    </div>



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing mt-5" >

        @yield('content')
    </div><!-- /.container -->

    <!-- FOOTER -->
    <footer class="footer-custom " style=" position: unset !important; bottom:unset !important ">
        <p>
            &copy; 2022-2023 La Restaurant Tunisienne<br/
                    <small>La Restaurant 100% made in Tunisia<br/>
                           <a href="#">Privacy</a> &middot; <a href="#">Terms</a></small>
        </p>
          </footer>
</main>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.js') }}"></script>
<script type="text/javascript">

    $('.test, .nav-link, .navbar-brand, .new-button').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(sectionTo).offset().top
        }, 1500);
    });
</script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<script type="text/javascript">

    function placeFooter() {
        if( $(document.body).height() < $(window).height() ) {
            $("footer").css({position: "fixed", bottom:"0px"});
        } else {
            $("footer").css({position: ""});
        }
    }

    placeFooter();

</script>
</body>
</html>
