@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-md-3" style="margin-left: -70px;
    margin-right: 66px; margin-top: 100px;">

            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    Filtrer
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="select">
                            <header class="card-header">

                                <div class="select">
                                    <select onchange="window.location.href = this.value" class="form-select">
                                        <option value="{{ route('showmenu') }}" @unless($slug) selected

                                            @endunless>Toutes catégories</option>


                                        <option value="Food" {{ $slug == "Food" ? 'selected' : '' }}>Food</option>
                                        <option value="Hot Drink" {{ $slug == "Hot Drink" ? 'selected' : '' }}>Hot Drink</option>
                                        <option value="Cold Drink" {{ $slug == "Cold Drink" ? 'selected' : '' }}>Cold Drink</option>


                                    </select>
                                </div>
                            </header>

                        </div>

                    </li>

                </ul>
            </div>


        </div>
        <div class="col-md-9">
            <h1 style="font-family:'Dancing Script', cursive  !important;font-size: 67px;
    text-align: center; "class=" animate__animated animate__backInLeft">Our Menus</h1>
            <div class="row">
                @foreach($items as $item)
                    <div class="col-md-4">
                        <div class="product-item text-center">
                            <a href="{{ route('showm',$item->id)  }}" ><img src="/images/{{ $item->image}}" class="img-fluid"/></a>
                            <h5>{{ $item->title }}</h5>
                            <span class="product-subtitle">{{ $item->title}}</span>
                            <span class="product-price">{{ ( $item->price /100) |number_format(2)}} £</span>
                        </div>



                    </div>

                @endforeach
            </div>
            <div class="navigation" style="margin-top: 100px">
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
