@extends('welcome')
@section('content')
<h1>Mon compte</h1>
Bienvenue {{ Auth::user()->name }} dans votre compte.<br/>
C'est dans cet espace que vous allez pouvoir gérer toutes vos informations personnelles.
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="account-item text-center">
            <img src="/images/password.png" alt="Modifier mon mot de passe"><br/>
            <a href="{{ route('modif') }}">Modifier mon mot de passe</a>
        </div>
    </div>

</div>
<hr>
@endsection
