@extends('welcome')

@section('content')
    @if(session()->has('info'))
        <div class="notification is-success">
            {{ session('info') }}
        </div>
        @endif
<h1 style="font-family:'Dancing Script', cursive  !important;font-size: 67px;
    text-align: center; "class=" animate__animated animate__backInLeft">Items Liste</h1>

<a href="{{ route('item.create') }}" class="btn btn-outline-dark" style="float: right;margin-bottom: 22px;">Create new</a>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Title</th>

        <th>Description</th>
        <th>Image</th>
        <th>Price</th>
        <th>CreatedBy</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
    <tr>


        <td>{{ $item->title }}</td>
        <td>{{ $item->description  }}</td>
        <td><img src="/images/{{ $item->image }}" height="100px"></td>
        <td>{{ ($item->price) |number_format(2)}}</td>
        <td>{{ $item->User->name }}</td>
        <td>
            <a href="{{ route('item.show',$item->id ) }}" class="btn btn-outline-primary" style="width: 80px">Show</a>
            <a href="{{ route('item.edit',$item->id) }}" class="btn btn-outline-warning" style="width:80px;">Edit</a>

            @if($item->deleted_at)
                <form action="{{ route('item.restore', $item->id) }}"

                      method="post">

                    @csrf
                    @method('PUT')
                    <button style="margin-left: 170px;margin-top: -67px;" class="btn btn-outline-success"

                            type="submit">Restaurer</button>
                </form>


                <form action="{{
route('item.force.destroy', $item->id) }}"
                      method="post">
                    @csrf
                    @method('DELETE')

                    <button
                        class="btn btn-outline-danger" style="width:200px;margin-top: -114px;margin-left: 267px"
                        type="submit">Supprimer definitivement</button>
                </form>
                @else


                <form action="{{
route('item.destroy', $item->id) }}"
                      method="post">
                    @csrf
                    @method('DELETE')

                    <button
                        class="btn btn-outline-danger" style="width:100px;margin-top: -67px;margin-left:170px"
                        type="submit">Supprimer</button>
                </form>
            @endif
        </td>


    </tr>

@endforeach

    </tbody>

</table>
<div class="navigation">
    {{ $items->links() }}
</div>


@endsection




