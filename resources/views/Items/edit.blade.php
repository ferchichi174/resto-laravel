@extends('welcome')

@section('content')
<h1>Edit Items</h1>

<form  method="post" action="{{ route('item.update',$item->id) }}" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div id="items">
        <div class="form-group"><label for="items_title" class="required">Title</label><input type="text" id="title" name="title" required="required" maxlength="255" class="form-control" value="{{ $item->title }}"></div>
        <div class="form-group"><label for="items_description" class="required">Description</label><input type="text" id="items_description" name="description" required="required" maxlength="255" class="form-control" value="{{ $item->description }}"></div>
        <div class="form-group">
            <label for="items_image">Image</label>
            <div class="custom-file"><input type="file" id="items_image" name="image" lang="en" class="custom-file-input"><label for="items_image" class="custom-file-label"></label>
            </div>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="1" name="isbest" @if ($item->isbest == 1)checked @endif>
            <label class="form-check-label" for="defaultCheck1">
                IsBest
            </label>
        </div>
        <div class="form-group"><label for="items_price" class="required">Price</label><input type="text" id="items_price" name="price" required="required" class="form-control" value="{{ $item->price }}"></div>
        <div class="form-group">
            <label class="" for="items_menu">Menu</label>

            <select id="items_menu" name="itemsmenu" class="form-control">
                @foreach( $menuss as $menusss)

                    <option  @if ( $menu == $menusss->title ) selected @endif  value="{{ $menusss->id }}"  >{{ $menusss->title }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <button class="btn btn-outline-primary" style="    margin: -21px;
      margin-top: 34px;">Update</button>
</form>

<a href="{{ route('item.index') }}" Class="btn btn-outline-secondary" style="    margin-top: -21px;
    margin-left: 115px;">back to list</a>

{{--{{ include('items/_delete_form.html.twig') }}--}}
@endsection
