@extends('welcome')

@section('content')



<div class="row">
    <div class="col-md-5">
        <img src="/images/{{ $item->image }}" alt="{{ $item->title }}" class="img-fluid">
    </div>
    <div class="col-md-7 my-auto">
        <h3>{{ $item->title }}</h3>
        <p>{{ $item->title }}</p>
        <span class="product-page-price">{{ ($item->price)|number_format(2) }} €</span>
        <hr>
        <p>
            {{ $item->description }}
        </p>
    </div>
</div>
<hr>
@auth()
<div style="text-align: center">
    <a href="{{ route('item.index') }}"class="btn btn-outline-primary" style="">back to list</a>

    <a href="{{ route('item.edit', $item->id) }}" class="btn btn-outline-warning" style="">edit</a>

{{--    {{ include('items/_delete_form.html.twig') }}--}}
</div>
@endauth
       <h3>Nos meilleures ventes</h3>
        <p>Découvrez les articles les plus vendus.</p>
        <div class="row">
{{--                {% for menu in menus %}--}}
                    <div class="col-md-3">
{{--                           {% include 'item/single_item.html.twig' %}#}--}}
                       </div>

        </div><!-- /.row -->




@endsection
