@extends('welcome')
@section('carousel')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="/images/slider.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/images/slider2.jpg" alt="Second slide">
            </div>
{{--            <div class="carousel-item">--}}
{{--                <img class="d-block w-100" src="/images/3.jpg" alt="Third slide">--}}
{{--            </div>--}}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        @endsection
@section('content')

    <h3 id="Menu">Nos meilleures Menus </h3>
    <p>Découvrez les Menus</p>
    <div class="row " >
@foreach($menus as $menu)
        <div class="col-md-3">
            <div class="product-item text-center">
                <a href="{{ route('showm',$menu->id) }}"><img src="/images/{{$menu->image}}"  style="border-radius: 130px;height: 253px;    width: 244px;
    margin-right: 10px;" ></a>
                <h5></h5>
                <span class="product-subtitle">{{ $menu->price }}</span>
                <span class="product-subtitle">{{ $menu->title }}</span>

            </div>
        </div>
        @endforeach
    </div><!-- /.row -->



    <!-- Three columns of text below the carousel -->



    <!-- START THE FEATURETTES -->
    <hr class="featurette-divider" id="About">

    <div class="row featurette" >
        <div class="col-md-7">
            <h2 class="featurette-heading">Une équipe de professionnel <span class="text-muted">au grand savoir-faire</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
            <img src="{{ asset('assets/img/About-C-bg.jpg') }}" class="img-fluid" alt="Un savoir-faire Français">
        </div>
    </div>
    <h3 id="Items">Nos meilleures items </h3>
    <p>Découvrez les items</p>
    <div class="row" >
        @foreach ($items as $item )
        <div class="col-md-3">
            <div class="product-item text-center">
                <a href="{{ route('showit',$item) }}"><img src="/images/{{ $item->image }}" alt="{{ $item->title }}" class="img-fluid" style="border-radius: 130px;height: 253px;"></a>
                <h5>{{ $item->title }}</h5>
                <span class="product-subtitle">{{ $item->price }}</span>
                <span class="product-subtitle">{{ $item->title }}</span>

            </div>
        </div>
        @endforeach
    </div>
    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Venez nous rencontrer dans nos <span class="text-muted">restaurant en Tunisie.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5 order-md-1">
            <img src="{{ asset('assets/img/GPtE-bg.jpg') }}" class="img-fluid" alt="Une boutique ouverte toute l'année">
        </div>
    </div>


@endsection
