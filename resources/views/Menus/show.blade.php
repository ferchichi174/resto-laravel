@extends('welcome')

@section('content')
<div class="row">
    <div class="col-md-5">
        <img src="/images/{{ $menu->image }}" alt="{{ $menu->title }}" class="img-fluid">
    </div>
    <div class="col-md-7 my-auto">
        <h3>{{ $menu->title }}</h3>
        <p>{{ $menu->title }}</p>
        <span class="product-page-price">{{ ($menu->price / 100)|number_format(2) }} €</span>
        <hr>
        <p>
            {{ $menu->description }}
        </p>
    </div>
</div>
<hr>
<div style="text-align: center">
    @auth()
    <a href="{{ route('menu.index') }}"class="btn btn-outline-primary" style="">back to list</a>

    <a href="{{ route('menu.edit', $menu->id) }}" class="btn btn-outline-warning" style="">edit</a>
    @endauth
{{--    {{ include('items/_delete_form.html.twig') }}--}}
</div>
       <h3>Nos meilleures ventes</h3>
        <p>Découvrez les articles les plus vendus.</p>
<div class="row">

    @foreach($menu->item as $items)
        <div class="col-md-3">
            <div class="product-item text-center">
                <a href=""><img src="/images/{{ $items->image }}" alt="{{ $items->title }}" class="img-fluid" style="border-radius: 130px;height: 253px;"></a>
                <h5>{{ $items->title }}</h5>
                <span class="product-subtitle">{{ $items->price }}</span>
                <span class="product-subtitle">{{ $items->title }}</span>

            </div>
        </div>
    @endforeach
</div><!-- /.row -->




@endsection
