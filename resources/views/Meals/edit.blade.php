@extends('welcome')

@section('content')
<h1>Edit Menus </h1>

<form  method="post" action="{{ route('meal.update',$meals->id) }}" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div id="items">
        <div class="form-group"><label for="items_title" class="required">Title</label><input type="text" id="title" name="title" required="required" maxlength="255" class="form-control" value="{{ $meals->title }}"></div>
        <div class="form-group"><label for="items_description" class="required">Description</label><input type="text" id="items_description" name="description" required="required" maxlength="255" class="form-control" value="{{ $meals->description }}"></div>
        <div class="form-group">
            <label for="items_image">Image</label>
            <div class="custom-file"><input type="file" id="items_image" name="image" lang="en" class="custom-file-input"><label for="items_image" class="custom-file-label"></label>
            </div>
        </div>

        <div class="form-group">
            <label class="" for="items_menu">Menu</label>

            <select id="items_menu" name="itemsmenu[]" class="form-control" multiple>

                    @foreach ($allItem as $items )
                <option  value="{{ $items->id }}" >
                    {{$items->title}}
                    @endforeach
                </option>

            </select>
        </div>
    </div>
    <button class="btn btn-outline-primary" style="    margin: -21px;
      margin-top: 34px;">Update</button>
</form>

<a href="{{ route('meal.index') }}" Class="btn btn-outline-secondary" style="    margin-top: -21px;
    margin-left: 115px;">back to list</a>

{{--{{ include('items/_delete_form.html.twig') }}--}}
@endsection
