@extends('welcome')

@section('content')
<div class="row">
    <div class="col-md-5">

        <img src="/images/{{ $meals->image }}" alt="{{ $meals->title }}" class="img-fluid">
    </div>
    <div class="col-md-7 my-auto">
        <h3>{{ $meals->title }}</h3>
        <p>{{ $meals->title }}</p>
        <span class="product-page-price">{{ ($meals->price / 100)|number_format(2) }} €</span>
        <hr>
        <p>
            {{ $meals->description }}
        </p>
    </div>
</div>
<hr>
<div style="text-align: center">
    @auth()
    <a href="{{ route('meal.index') }}"class="btn btn-outline-primary" style="">back to list</a>
    @endauth

</div>
       <h3>Meal Items</h3>
        <div class="row">

            @foreach($meals->item as $items)
                    <div class="col-md-3">
                        <div class="product-item text-center">
                            <a href=""><img src="/images/{{ $items->image }}" alt="{{ $items->title }}" class="img-fluid" style="border-radius: 130px;height: 253px;"></a>
                            <h5>{{ $items->title }}</h5>
                            <span class="product-subtitle">{{ $items->price }}</span>
                            <span class="product-subtitle">{{ $items->title }}</span>

                        </div>
                       </div>
            @endforeach
        </div><!-- /.row -->




@endsection
