<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MealsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'price' =>$this->faker->randomFloat($nbMaxDecimals = NULL, $min = 10, $max = 100),
            'description' => $this->faker->text,
            'image' => 'menu'.rand(1,7).".jpg",

        ];
    }
}
