<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types=["Food","Cold Drink","Hot Drink"];
        return [
            'title'=>$this->faker->sentence(2,true),
            'type' =>$types[rand(0,2)],
            'description' =>$this->faker->text,

            'image' => 'menu'.rand(1,7).".jpg",
        ];
    }
}
