<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'price' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 10, $max = 100),
            'description' => $this->faker->text(),
            'image' => 'item'.rand(1,7).".jpg",
        ];
    }
}
